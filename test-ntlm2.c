/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#include <ntlm2.h>

#include <stdio.h>		/* printf */
#include <string.h>		/* memcmp */

static const char expected_type1[] =
  /* NTLMSSP signature */
  "\x4e\x54\x4c\x4d\x53\x53\x50\x00"
  /* Type-1 */
  "\x01\x00\x00\x00"
  /* flags */
  "\x07\xb2\x00\x00"
  /* domain */
  "\x06\x00\x06\x00\x20\x00\x00\x00"
  /* workstation */
  "\x0b\x00\x0b\x00\x26\x00\x00\x00"
  /* data container */
  "\x44\x4f\x4d\x41\x49\x4e" "\x57\x4f\x52\x4b\x53\x54\x41\x54\x49\x4f\x4e";

static const char type2[] =
  /* NTLMSSP signature */
  "\x4e\x54\x4c\x4d\x53\x53\x50\x00"
  /* Type-2 */
  "\x02\x00\x00\x00"
  /* Target name */
  "\x0c\x00\x0c\x00\x30\x00\x00\x00"
  /* Flags */
  "\x01\x02\x81\x00"
  /* Challenge */
  "\x01\x23\x45\x67\x89\xab\xcd\xef"
  /* Context */
  "\x00\x00\x00\x00\x00\x00\x00\x00"
  /* Target Information */
  "\x62\x00\x62\x00\x3c\x00\x00\x00"
  /* Target Name */
  "\x44\x00\x4f\x00\x4d\x00\x41\x00\x49\x00\x4e\x00"
  /* Target Information Data */
  "\x02\x00\x0c\x00\x44\x00\x4f\x00\x4d\x00\x41\x00\x49\x00\x4e\x00"
  "\x01\x00\x0c\x00\x53\x00\x45\x00\x52\x00\x56\x00\x45\x00\x52\x00"
  "\x04\x00\x14\x00\x64\x00\x6f\x00\x6d\x00\x61\x00\x69\x00\x6e\x00\x2e\x00\x63\x00\x6f\x00\x6d\x00"
  "\x03\x00\x22\x00\x73\x00\x65\x00\x72\x00\x76\x00\x65\x00\x72\x00\x2e\x00\x64\x00\x6f\x00\x6d\x00\x61\x00\x69\x00\x6e\x00\x2e\x00\x63\x00\x6f\x00\x6d\x00"
  "\x00\x00\x00\x00";

static const char expected_type3[] =
  /* 0-7: NTLMSSP signature */
  "\x4e\x54\x4c\x4d\x53\x53\x50\x00"
  /* 8-11: Type-3 */
  "\x03\x00\x00\x00"
  /* 12-19: LM response buffer */
  "\x18\x00\x18\x00\x6a\x00\x00\x00"
  /* 20-27: NTLM response buffer */
  "\x18\x00\x18\x00\x82\x00\x00\x00"
  /* 28-35: Target Name buffer */
  "\x0c\x00\x0c\x00\x40\x00\x00\x00"
  /* 36-43: Username buffer */
  "\x08\x00\x08\x00\x4c\x00\x00\x00"
  /* 44-51: Workstation buffer */
  "\x16\x00\x16\x00\x54\x00\x00\x00"
  /* 52-59: Session key buffer */
  "\x00\x00\x00\x00\x9a\x00\x00\x00"
  /* 60-63: Flags */
  "\x00\x00\x00\x00"
  /* 64-75: Target Name Data: "DOMAIN" */
  "\x44\x00\x4f\x00\x4d\x00\x41\x00\x49\x00\x4e\x00"
  /* 76-83: Username Data: "user" */
  "\x75\x00\x73\x00\x65\x00\x72\x00"
  /* 84-105: Workstation Data: "WORKSTATION" */
  "\x57\x00\x4f\x00\x52\x00\x4b\x00\x53\x00\x54"
  "\x00\x41\x00\x54\x00\x49\x00\x4f\x00\x4e\x00"
  /* 106-129 LM response Data */
  "\xc3\x37\xcd\x5c\xbd\x44\xfc\x97"
  "\x82\xa6\x67\xaf\x6d\x42\x7c\x6d"
  "\xe6\x7c\x20\xc2\xd3\xe7\x7c\x56"
  /* 130-153 NTLM response Data */
  "\x25\xa9\x8c\x1c\x31\xe8\x18\x47"
  "\x46\x6b\x29\xb2\xdf\x46\x80\xf3"
  "\x99\x58\xfb\x8c\x21\x3a\x9c\xc6";

int
main (void)
{
  int rc;
  ntlm2c_t auth;
  char *out;
  size_t len;
  size_t i;

  rc = ntlm2_global_init (0);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2_global_init failed %d\n", rc);
      return 1;
    }

  rc = ntlm2_client_init (&auth, 0);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2_client_init failed %d\n", rc);
      return 1;
    }

  rc = ntlm2c_domain (auth, "DOMAIN");
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_domain failed %d\n", rc);
      return 1;
    }

  rc = ntlm2c_workstation (auth, "WORKSTATION");
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_workstation failed %d\n", rc);
      return 1;
    }

  rc = ntlm2c_type1 (auth, &out, &len);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_type1 failed %d\n", rc);
      return 1;
    }

  if (len != sizeof (expected_type1) - 1
      || memcmp (out, expected_type1, len) != 0)
    {
      printf ("type1: ");
      for (i = 0; i < len; i++)
	printf ("%02x ", out[i] & 0xFF);
      printf ("\n");

      printf ("ntlm2c_type1 unexpected message\n");
      return 1;
    }

  rc = ntlm2c_type2 (auth, type2, sizeof (type2));
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_type2 failed %d\n", rc);
      return 1;
    }

  rc = ntlm2c_username (auth, "user");
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_username failed %d\n", rc);
      return 1;
    }

  rc = ntlm2c_type3_passwd (auth, "SecREt01", &out, &len);
  if (rc != NTLM2_OK)
    {
      printf ("ntlm2c_type3_passwd failed %d\n", rc);
      return 1;
    }

  if (len != sizeof (expected_type3) - 1
      || memcmp (out, expected_type3, len) != 0)
    {
      printf ("type3: ");
      for (i = 0; i < len; i++)
	printf ("%02x ", out[i] & 0xFF);
      printf ("\n");

      printf ("ntlm2c_type3 unexpected message\n");
      return 1;
    }

  ntlm2_client_done (auth);

  ntlm2_global_done ();

  return 0;
}
