/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#include <stdint.h>		/* uint64_t */

extern uint64_t _ntlm2_des_encrypt (uint64_t key, uint64_t plaintext);
