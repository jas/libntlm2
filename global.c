/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#include "ntlm2.h"

/**
 * ntlm2_global_init:
 * @options: for future expansion, currently must be 0.
 *
 * Initialize the NTLM2 library.  This function must be called
 * successfully before using any other function from the library.
 *
 * Returns: %NTLM2_OK on success, or %NTLM2_CALL_ERROR when an unknown
 *   @options was specified.
 **/
int
ntlm2_global_init (int options)
{
  if (options != 0)
    return NTLM2_CALL_ERROR;

  /* Not much here now -- but I anticipate that this function may
     initialize some external crypto library eventually.  The flags
     are often useful to influence how the crypto library is
     initialized.  */

  return NTLM2_OK;
}

/**
 * ntlm2_global_done:
 *
 * Call when the NTLM2 library is not needed any more to clean up any
 * global state.
 **/
void
ntlm2_global_done (void)
{
  return;
}
