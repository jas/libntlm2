/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#include <stddef.h>		/* size_t */

#define	_NTLM2_MD4_DIGEST_LENGTH 16

extern void _ntlm2_md4 (unsigned char *data, size_t length,
			unsigned char digest[_NTLM2_MD4_DIGEST_LENGTH]);
