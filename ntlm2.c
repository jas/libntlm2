/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#include "ntlm2.h"

#include <stdint.h>		/* uint_least32_t */
#include <stdlib.h>		/* malloc */
#include <string.h>		/* memcpy */
#include <stdbool.h>		/* bool */
#include <stdio.h>		/* printf */

#include "des.h"
#include "md4.h"

enum
{
  NTLM2_FLAGS_UNICODE = 0x00000001,	/* bit 1 */
  NTLM2_FLAGS_OEM = 0x00000002,	/* bit 2 */
  NTLM2_FLAGS_REQUEST_TARGET = 0x00000004,	/* bit 3 */
  /* NTLM2_UNKNOWN0                     = 0x00000008, *//* bit 4 */
  /* NTLM2_FLAGS_SIGN                   = 0x00000010, *//* bit 5 */
  /* NTLM2_FLAGS_SEAL                   = 0x00000020, *//* bit 6 */
  /* NTLM2_FLAGS_DATAGRAM_STYLE         = 0x00000040, *//* bit 7 */
  /* NTLM2_FLAGS_LANMAN_KEY             = 0x00000080, *//* bit 8 */
  /* NTLM2_FLAGS_NETWARE                = 0x00000100, *//* bit 9 */
  NTLM2_FLAGS_NTLM = 0x00000200,	/* bit 10 */
  /* NTLM2_FLAGS_UNKNOWN1               = 0x00000400, *//* bit 11 */
  /* NTLM2_FLAGS_ANONYMOUS              = 0x00000800, *//* bit 12 */
  NTLM2_FLAGS_DOMAIN_SUPPLIED = 0x00001000,	/* bit 13 */
  NTLM2_FLAGS_WORKSTATION_SUPPLIED = 0x00002000,	/* bit 14 */
  /* NTLM2_FLAGS_LOCAL                  = 0x00004000, *//* bit 15 */
  NTLM2_FLAGS_ALWAYS_SIGN = 0x00008000,	/* bit 16 */
  /* NTLM2_FLAGS_DOMAIN_TARGET          = 0x00010000, *//* bit 17 */
  /* NTLM2_FLAGS_SERVER_TARGET          = 0x00020000, *//* bit 18 */
  /* NTLM2_FLAGS_SHARE_TARGET           = 0x00040000, *//* bit 19 */
  /* NTLM2_FLAGS_NTLM2_KEY              = 0x00080000, *//* bit 20 */
  /* NTLM2_FLAGS_UNKNOWN2               = 0x00100000, *//* bit 21 */
  /* NTLM2_FLAGS_UNKNOWN3               = 0x00200000, *//* bit 22 */
  /* NTLM2_FLAGS_UNKNOWN4               = 0x00300000, *//* bit 23 */
  /* NTLM2_FLAGS_TARGET_INFO            = 0x00800000, *//* bit 24 */
  /* NTLM2_FLAGS_128BIT                 = 0x20000000, *//* bit 25 */
  /* NTLM2_FLAGS_KEY_EXCHANGE           = 0x40000000, *//* bit 26 */
  /* NTLM2_FLAGS_56BIT                  = 0x80000000  *//* bit 27 */
} ntlm2_flags;

#define NTLM2C_DEFAULT_FLAGS_NTLM		\
  (NTLM2_FLAGS_UNICODE |			\
   NTLM2_FLAGS_OEM |				\
   NTLM2_FLAGS_REQUEST_TARGET |			\
   NTLM2_FLAGS_NTLM |				\
   NTLM2_FLAGS_ALWAYS_SIGN)

struct ntlm2c_st
{
  int options;
  uint_least32_t flags;
  char *domain;
  char *workstation;
  char *username;
  char *target;
  char *challenge;
};

static const char ntlmssp[] = "NTLMSSP";

/**
 * ntlm2_client_init:
 * @auth: output variable holding pointer to newly allocate handle
 * @options: binary OR of #ntlm2_client_options members
 *
 * Allocate a NTLM client handle.
 *
 * The @flags are used as follows: %NTLM2_NO_TYPE2_VALIDATION means
 * that no input validation of the Type-2 message is done beyond the
 * bare minimum required to form a proper Type-3 message.
 *
 * Returns: %NTLM2_OK on success or %NTLM2_MALLOC_ERROR on malloc
 *   failure.
 **/
int
ntlm2_client_init (ntlm2c_t * auth, int options)
{
  if ((options & ~NTLM2_NO_TYPE2_VALIDATION) != 0)
    return NTLM2_CALL_ERROR;

  *auth = malloc (sizeof (**auth));
  if (*auth == NULL)
    return NTLM2_MALLOC_ERROR;

  (*auth)->options = options;
  (*auth)->flags = NTLM2C_DEFAULT_FLAGS_NTLM;
  (*auth)->domain = NULL;
  (*auth)->workstation = NULL;
  (*auth)->username = NULL;
  (*auth)->target = NULL;
  (*auth)->challenge = NULL;

  return NTLM2_OK;
}

/**
 * ntlm2_client_done:
 * @auth: client NTLM handle allocated with ntlm2_client_init()
 *
 * Deallocate NTLM client handle.
 **/
void
ntlm2_client_done (ntlm2c_t auth)
{
  free (auth->domain);
  free (auth->workstation);
  free (auth->username);
  free (auth->target);
  free (auth->challenge);
  free (auth);
}

/**
 * ntlm2c_domain:
 * @auth: client NTLM handle allocated with ntlm2_client_init()
 * @domain: zero-terminated string holding name of domain
 *
 * Set the domain for the Type-1 authentication message.  This is the
 * domain in which the workstation has membership.
 *
 * Returns: %NTLM2_OK on success or %NTLM2_MALLOC_ERROR on malloc
 *   failure.
 **/
int
ntlm2c_domain (ntlm2c_t auth, const char *domain)
{
  free (auth->domain);

  auth->domain = strdup (domain);
  if (auth->domain == NULL)
    return NTLM2_MALLOC_ERROR;

  auth->flags |= NTLM2_FLAGS_DOMAIN_SUPPLIED;

  return NTLM2_OK;
}

/**
 * ntlm2c_workstation:
 * @auth: client NTLM handle allocated with ntlm2_client_init()
 * @workstation: zero-terminated string holding name of workstation
 *
 * Set the workstation for the Type-1 authentication message.  This is
 * the client's workstation name.
 *
 * Returns: %NTLM2_OK on success or %NTLM2_MALLOC_ERROR on malloc
 *   failure.
 **/
int
ntlm2c_workstation (ntlm2c_t auth, const char *workstation)
{
  free (auth->workstation);

  auth->workstation = strdup (workstation);
  if (auth->workstation == NULL)
    return NTLM2_MALLOC_ERROR;

  auth->flags |= NTLM2_FLAGS_WORKSTATION_SUPPLIED;

  return NTLM2_OK;
}

/**
 * ntlm2c_username:
 * @auth: client NTLM handle allocated with ntlm2_client_init()
 * @username: zero-terminated string holding name of user
 *
 * Set the username for the Type-3 authentication message.
 *
 * Returns: %NTLM2_OK on success or %NTLM2_MALLOC_ERROR on malloc
 *   failure.
 **/
int
ntlm2c_username (ntlm2c_t auth, const char *username)
{
  free (auth->username);

  auth->username = strdup (username);
  if (auth->username == NULL)
    return NTLM2_MALLOC_ERROR;

  return NTLM2_OK;
}

/**
 * ntlm2c_target:
 * @auth: client NTLM handle allocated with ntlm2_client_init()
 * @target: zero-terminated string holding domain of user
 *
 * Set the target for the Type-3 authentication message.  This is the
 * authentication realm in which the authenticating account has
 * membership.  If not supplied, ntlm2c_type3_passwd() will default to
 * using the domain given by ntlm2c_domain().
 *
 * Returns: %NTLM2_OK on success or %NTLM2_MALLOC_ERROR on malloc
 *   failure.
 **/
int
ntlm2c_target (ntlm2c_t auth, const char *target)
{
  free (auth->target);

  auth->target = strdup (target);
  if (auth->target == NULL)
    return NTLM2_MALLOC_ERROR;

  return NTLM2_OK;
}

static char *
write_uint16 (char *p, uint16_t i)
{
  p[0] = i & 0xFF;
  p[1] = (i >> 8) & 0xFF;
  return p + 2;
}

static char *
write_uint32 (char *p, uint32_t i)
{
  p[0] = i & 0xFF;
  p[1] = (i >> 8) & 0xFF;
  p[2] = (i >> 16) & 0xFF;
  p[3] = (i >> 24) & 0xFF;
  return p + 4;
}

static uint32_t
read_uint32 (const char *p)
{
  uint32_t i;
  i = (p[0] & 0xFF)
    | ((p[1] & 0xFF) << 8) | ((p[2] & 0xFF) << 16) | ((p[3] & 0xFF) << 24);
  return i;
}

static uint64_t
read_uint64 (const unsigned char *p)
{
  uint64_t i;
  i = ((uint64_t) (p[7] & 0xFF) << 0)
    | ((uint64_t) (p[6] & 0xFF) << 8)
    | ((uint64_t) (p[5] & 0xFF) << 16)
    | ((uint64_t) (p[4] & 0xFF) << 24)
    | ((uint64_t) (p[3] & 0xFF) << 32)
    | ((uint64_t) (p[2] & 0xFF) << 40)
    | ((uint64_t) (p[1] & 0xFF) << 48) | ((uint64_t) (p[0] & 0xFF) << 56);
  return i;
}

static void
write_uint64 (unsigned char *p, uint64_t i)
{
  p[7] = i & 0xFF;
  p[6] = (i >> 8) & 0xFF;
  p[5] = (i >> 16) & 0xFF;
  p[4] = (i >> 24) & 0xFF;
  p[3] = (i >> 32) & 0xFF;
  p[2] = (i >> 40) & 0xFF;
  p[1] = (i >> 48) & 0xFF;
  p[0] = (i >> 56) & 0xFF;
}

/**
 * ntlm2c_type1:
 * @auth: client NTLM handle allocated with ntlm2_client_init()
 * @out: output variable holding pointer to newly allocated string
 * @len: output variable with length of *@out
 *
 * Generate the initial NTLM client message, also known as a Type-1
 * message or an authentication request message.  This is the first
 * message sent from the client to the server.
 *
 * The caller is responsible for deallocating the @out buffer.
 *
 * Returns: %NTLM2_OK on success, or %NTLM2_MALLOC_ERROR on malloc
 *   failure, or %NTLM2_CALL_ERROR if @len was NULL.
 **/
int
ntlm2c_type1 (ntlm2c_t auth, char **out, size_t * len)
{
  bool domain_p = (auth->flags & NTLM2_FLAGS_DOMAIN_SUPPLIED) != 0 &&
    auth->domain != NULL;
  bool workstation_p = (auth->flags & NTLM2_FLAGS_WORKSTATION_SUPPLIED) != 0
    && auth->workstation != NULL;
  size_t domainlen = auth->domain ? strlen (auth->domain) : 0;
  size_t workstationlen = auth->workstation ? strlen (auth->workstation) : 0;
  size_t offset;
  char *p;

  if (len == NULL)
    return NTLM2_CALL_ERROR;

  /* Type-1 messages looks like this:
   *
   * byte    Description
   * 0-7     NTLMSSP signature
   * 8-11    Message type (1)
   * 12-15   Flags
   * (16-23) optional domain
   * (24-31) optional workstation
   * (32-39) optional os structure
   * (-)     data block
   *
   * We include domain + workstation if supplied, but not OS
   * structure.
   */

  *len = sizeof (ntlmssp) + 2 * sizeof (uint32_t);
  offset = *len;
  if (domain_p)
    {
      *len += 2 * sizeof (uint16_t) + sizeof (uint32_t) + domainlen;
      offset += 2 * sizeof (uint16_t) + sizeof (uint32_t);
    }
  if (workstation_p)
    {
      *len += 2 * sizeof (uint16_t) + sizeof (uint32_t) + workstationlen;
      offset += 2 * sizeof (uint16_t) + sizeof (uint32_t);
    }

  if (out == NULL)
    return NTLM2_OK;

  *out = p = malloc (*len);
  if (p == NULL)
    return NTLM2_MALLOC_ERROR;

  memcpy (p, ntlmssp, sizeof (ntlmssp));
  p += sizeof (ntlmssp);
  p = write_uint32 (p, 1);
  p = write_uint32 (p, auth->flags);

  if (domain_p)
    {
      p = write_uint16 (p, domainlen);
      p = write_uint16 (p, domainlen);
      p = write_uint32 (p, offset);
      memcpy (*out + offset, auth->domain, domainlen);
      offset += domainlen;
    }

  if (workstation_p)
    {
      p = write_uint16 (p, workstationlen);
      p = write_uint16 (p, workstationlen);
      p = write_uint32 (p, offset);
      memcpy (*out + offset, auth->workstation, workstationlen);
      offset += workstationlen;
    }

  return NTLM2_OK;
}

#define NTLM2_TYPE2_CHALLENGE_OFFSET 24
#define NTLM2_TYPE2_CHALLENGE_LENGTH 8

/**
 * ntlm2c_type2:
 * @auth: client NTLM handle allocated with ntlm2_client_init()
 * @type2: input variable with Type-2 message
 * @type2len: length of @type2
 *
 * Receive a Type-2 message, also known as a the server challenge,
 * from the server.  If the @auth handle was initialized with the
 * %NTLM2_NO_TYPE2_VALIDATION option, the Type-2 message is not
 * verified for correctness beyond what is required to parse out the
 * essential challenge data.
 *
 * Returns: %NTLM2_OK on success, or %NTLM2_MALLOC_ERROR on malloc
 *   failure.
 **/
int
ntlm2c_type2 (ntlm2c_t auth, const char *type2, size_t type2len)
{
  if (type2len < NTLM2_TYPE2_CHALLENGE_OFFSET + NTLM2_TYPE2_CHALLENGE_LENGTH)
    return NTLM2_TYPE2_INVALID;

  free (auth->challenge);

  auth->challenge = malloc (NTLM2_TYPE2_CHALLENGE_LENGTH);
  if (auth->challenge == NULL)
    return NTLM2_MALLOC_ERROR;

  memcpy (auth->challenge, type2 + NTLM2_TYPE2_CHALLENGE_OFFSET,
	  NTLM2_TYPE2_CHALLENGE_LENGTH);

  if (!(auth->options & NTLM2_NO_TYPE2_VALIDATION))
    {
      uint32_t flags;

      if (memcmp (type2, ntlmssp, sizeof (ntlmssp)) != 0)
	return NTLM2_TYPE2_INVALID;
      if (read_uint32 (type2 + sizeof (ntlmssp)) != 2)
	return NTLM2_TYPE2_INVALID;

      flags = read_uint32 (type2 + 20);

      if ((flags & NTLM2_FLAGS_NTLM) == 0)
	return NTLM2_TYPE2_INVALID;

      if ((flags & (NTLM2_FLAGS_UNICODE | NTLM2_FLAGS_OEM)) == 0)
	return NTLM2_TYPE2_INVALID;

      if ((flags & (NTLM2_FLAGS_UNICODE | NTLM2_FLAGS_OEM)) ==
	  (NTLM2_FLAGS_UNICODE | NTLM2_FLAGS_OEM))
	return NTLM2_TYPE2_INVALID;

      /* FIXME: We can do more sanity checking here. */
    }

  return NTLM2_OK;
}

#define PADPASSWDLEN 14

static void
ctoupper (unsigned char *p)
{
  switch (*p)
    {
    case 'a':
      *p = 'A';
      break;
    case 'b':
      *p = 'B';
      break;
    case 'c':
      *p = 'C';
      break;
    case 'd':
      *p = 'D';
      break;
    case 'e':
      *p = 'E';
      break;
    case 'f':
      *p = 'F';
      break;
    case 'g':
      *p = 'G';
      break;
    case 'h':
      *p = 'H';
      break;
    case 'i':
      *p = 'I';
      break;
    case 'j':
      *p = 'J';
      break;
    case 'k':
      *p = 'K';
      break;
    case 'l':
      *p = 'L';
      break;
    case 'm':
      *p = 'M';
      break;
    case 'n':
      *p = 'N';
      break;
    case 'o':
      *p = 'O';
      break;
    case 'p':
      *p = 'P';
      break;
    case 'q':
      *p = 'Q';
      break;
    case 'r':
      *p = 'R';
      break;
    case 's':
      *p = 'S';
      break;
    case 't':
      *p = 'T';
      break;
    case 'u':
      *p = 'U';
      break;
    case 'v':
      *p = 'V';
      break;
    case 'w':
      *p = 'W';
      break;
    case 'x':
      *p = 'X';
      break;
    case 'y':
      *p = 'Y';
      break;
    case 'z':
      *p = 'Z';
      break;
    default:
      break;
    }
}

/* C89 compliant way to cast 'char' to 'unsigned char'. */
static inline unsigned char
to_uchar (char ch)
{
  return ch;
}

static uint64_t
str2deskey (const unsigned char *key7)
{
  uint64_t i;
  i = ((uint64_t) (key7[0]) << 56)
    | ((uint64_t) (((key7[0] << 7) & 0xFF) | (key7[1] >> 1)) << 48)
    | ((uint64_t) (((key7[1] << 6) & 0xFF) | (key7[2] >> 2)) << 40)
    | ((uint64_t) (((key7[2] << 5) & 0xFF) | (key7[3] >> 3)) << 32)
    | ((uint64_t) (((key7[3] << 4) & 0xFF) | (key7[4] >> 4)) << 24)
    | ((uint64_t) (((key7[4] << 3) & 0xFF) | (key7[5] >> 5)) << 16)
    | ((uint64_t) (((key7[5] << 2) & 0xFF) | (key7[6] >> 6)) << 8)
    | ((uint64_t) ((key7[6] << 1) & 0xFF));
  return i;
}

/**
 * ntlm2_lmhash:
 * @passwd: input zero-terminated string of password
 * @out: pre-allocated buffer for output lmhash value, must have room
 *   for at least %NTLM2_HASH_LEN bytes
 *
 * Compute the LM-hash of a password.
 *
 * Returns: %NTLM2_OK on success, or an error code.
 **/
int
ntlm2_lmhash (const char *passwd, char *out)
{
  size_t passwdlen = strlen (passwd);
  char padded_passwd[PADPASSWDLEN];
  size_t i;
  uint64_t tmp;
  uint64_t kgs = read_uint64 ("KGS!@#$%");

  if (out == NULL)
    return NTLM2_CALL_ERROR;

  if (passwdlen > PADPASSWDLEN)
    passwdlen = PADPASSWDLEN;

  memcpy (padded_passwd, passwd, passwdlen);
  memset (padded_passwd + passwdlen, 0, PADPASSWDLEN - passwdlen);
  for (i = 0; i < PADPASSWDLEN; i++)
    ctoupper (padded_passwd + i);

  tmp = str2deskey (padded_passwd);
  tmp = _ntlm2_des_encrypt (tmp, kgs);
  write_uint64 (out, tmp);

  tmp = str2deskey (padded_passwd + 7);
  tmp = _ntlm2_des_encrypt (tmp, kgs);
  write_uint64 (out + 8, tmp);

  return NTLM2_OK;
}

/**
 * ntlm2_ntlmhash:
 * @passwd: input zero-terminated string of password
 * @out: pre-allocated buffer for output ntlmhash value, must have room
 *   for at least %NTLM2_HASH_LEN bytes
 *
 * Compute the NTLM-hash of a password.
 *
 * Returns: %NTLM2_OK on success, or an error code.
 **/
int
ntlm2_ntlmhash (const char *passwd, char *out)
{
  size_t passwdlen = strlen (passwd);
  char *unicode_passwd;
  size_t i;

  if (out == NULL)
    return NTLM2_CALL_ERROR;

  unicode_passwd = malloc (passwdlen * 2);
  if (unicode_passwd == NULL)
    return NTLM2_MALLOC_ERROR;

  for (i = 0; i < passwdlen; i++)
    {
      unicode_passwd[2 * i + 0] = passwd[i];
      unicode_passwd[2 * i + 1] = '\x00';
    }

  _ntlm2_md4 (unicode_passwd, 2 * passwdlen, out);

  free (unicode_passwd);

  return NTLM2_OK;
}

static void
hash2response (const unsigned char hash[21],
	       const unsigned char *challenge,
	       unsigned char *response)
{
  uint64_t tmp;
  uint64_t c = read_uint64 (challenge);

  /* 1 */
  tmp = str2deskey (hash);
  tmp = _ntlm2_des_encrypt (tmp, c);
  write_uint64 (response, tmp);

  /* 2 */
  tmp = str2deskey (hash + 7);
  tmp = _ntlm2_des_encrypt (tmp, c);
  write_uint64 (response + 8, tmp);

  /* 3 */
  tmp = str2deskey (hash + 14);
  tmp = _ntlm2_des_encrypt (tmp, c);
  write_uint64 (response + 16, tmp);
}

static int
gen_lm_response (const char *passwd, const char *challenge, char *lmresponse)
{
  unsigned char lmhash[21];
  int rc;

  rc = ntlm2_lmhash (passwd, lmhash);
  if (rc != NTLM2_OK)
    return rc;

  memset (lmhash + 16, 0, 5);
  hash2response (lmhash, challenge, lmresponse);

  return NTLM2_OK;
}

static int
gen_ntlm_response (const char *passwd,
		   const char *challenge, char *ntlmresponse)
{
  char ntlm_hash[21];
  int rc;

  rc = ntlm2_ntlmhash (passwd, ntlm_hash);
  if (rc != NTLM2_OK)
    return rc;

  memset (ntlm_hash + 16, 0, 5);
  hash2response (ntlm_hash, challenge, ntlmresponse);

  return NTLM2_OK;
}

#define RESPONSELEN 24

/**
 * ntlm2c_type3_passwd:
 * @auth: client NTLM handle allocated with ntlm2_client_init()
 * @passwd: input buffer with password used for authentication
 * @out: output variable holding pointer to newly allocated string
 * @len: output variable with length of *@out
 *
 * Generate the final NTLM client message, also known as a Type-3
 * message or an authentication response message.  This is the second
 * and last message sent from the client to the server.
 *
 * The caller is responsible for deallocating the @out buffer.
 *
 * Returns: %NTLM2_OK on success, or %NTLM2_MALLOC_ERROR on malloc
 *   failure, or %NTLM2_CALL_ERROR if @len was NULL.
 **/
int
ntlm2c_type3_passwd (ntlm2c_t auth, const char *passwd,
		     char **out, size_t * len)
{
  size_t offset;
  const char *target = auth->target;
  size_t targetlen = auth->target ? strlen (auth->target) : 0;
  size_t userlen = auth->username ? strlen (auth->username) : 0;
  size_t wslen = auth->workstation ? strlen (auth->workstation) : 0;
  size_t i;
  char *p;
  int rc;

  /* If target isn't supplied, default to use the same domain for the
     user as for the client's workstation. */
  if (auth->target == NULL)
    {
      target = auth->domain;
      targetlen = target ? strlen (target) : 0;
    }

  /* Type-3 messages looks like this:
   *
   * byte    Description
   * 0-7     NTLMSSP signature
   * 8-11    Message type (3)
   * 12-19   LMresponse
   * 20-27   NTLMresponse
   * 28-35   target name
   * 36-43   user name
   * 44-51   workstation name
   * (52-59) optional session key
   * (60-63) optional flags
   * (64-71) optional os structure
   * (-)     data block
   *
   * We include empty session key and flags, but not OS structure.
   */

  *len = sizeof (ntlmssp) + sizeof (uint32_t)
    + 6 * (2 * sizeof (uint16_t) + sizeof (uint32_t))
    + sizeof (uint32_t);
  offset = *len;
  *len += 2 * targetlen + 2 * userlen + 2 * wslen + 2 * RESPONSELEN;

  if (out == NULL)
    return NTLM2_OK;

  *out = p = malloc (*len);
  if (p == NULL)
    return NTLM2_MALLOC_ERROR;

  memcpy (p, ntlmssp, sizeof (ntlmssp));
  p += sizeof (ntlmssp);
  p = write_uint32 (p, 3);

  p = write_uint16 (p, RESPONSELEN);
  p = write_uint16 (p, RESPONSELEN);
  p = write_uint32 (p, *len - 2 * RESPONSELEN);

  rc = gen_lm_response (passwd, auth->challenge,
			*out + *len - 2 * RESPONSELEN);
  if (rc != NTLM2_OK)
    return rc;

  p = write_uint16 (p, RESPONSELEN);
  p = write_uint16 (p, RESPONSELEN);
  p = write_uint32 (p, *len - RESPONSELEN);

  rc = gen_ntlm_response (passwd, auth->challenge,
			  *out + *len - RESPONSELEN);
  if (rc != NTLM2_OK)
    return rc;

  p = write_uint16 (p, 2 * targetlen);
  p = write_uint16 (p, 2 * targetlen);
  p = write_uint32 (p, offset);
  offset += 2 * targetlen;

  p = write_uint16 (p, 2 * userlen);
  p = write_uint16 (p, 2 * userlen);
  p = write_uint32 (p, offset);
  offset += 2 * userlen;

  p = write_uint16 (p, 2 * wslen);
  p = write_uint16 (p, 2 * wslen);
  p = write_uint32 (p, offset);
  offset += 2 * wslen;

  p = write_uint16 (p, 0);
  p = write_uint16 (p, 0);
  p = write_uint32 (p, *len);

  p = write_uint32 (p, 0);

  for (i = 0; i < targetlen; i++)
    {
      *p++ = target[i];
      *p++ = '\x00';
    }

  for (i = 0; i < userlen; i++)
    {
      *p++ = auth->username[i];
      *p++ = '\x00';
    }

  for (i = 0; i < wslen; i++)
    {
      *p++ = auth->workstation[i];
      *p++ = '\x00';
    }

  return NTLM2_OK;
}
