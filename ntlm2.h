/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#ifndef NTLM2_H
# define NTLM2_H

# ifdef __cplusplus
extern "C"
{
# endif

# include <stddef.h>		/* size_t */

  /**
   * NTLM2_VERSION
   *
   * Pre-processor symbol with a string that describe the header file
   * version number.
   */
# define NTLM2_VERSION "0.0.0"

  /**
   * NTLM2_VERSION_MAJOR
   *
   * Pre-processor symbol with a decimal value that describe the major
   * level of the header file version number.  For example, when the
   * header version is 1.2.3 this symbol will be 1.
   */
# define NTLM2_VERSION_MAJOR 0

  /**
   * NTLM2_VERSION_MINOR
   *
   * Pre-processor symbol with a decimal value that describe the minor
   * level of the header file version number.  For example, when the
   * header version is 1.2.3 this symbol will be 2.
   */
# define NTLM2_VERSION_MINOR 0

  /**
   * NTLM2_VERSION_PATCH
   *
   * Pre-processor symbol with a decimal value that describe the patch
   * level of the header file version number.  For example, when the
   * header version is 1.2.3 this symbol will be 3.
   */
# define NTLM2_VERSION_PATCH 0

  /**
   * NTLM2_VERSION_NUMBER
   *
   * Pre-processor symbol with a hexadecimal value describing the
   * header file version number.  For example, when the header version
   * is 1.2.3 this symbol will have the value 0x010203.
   */
# define NTLM2_VERSION_NUMBER 0x000000

  /**
   * ntlm2_rc:
   * @NTLM2_OK: success (guaranteed to be the value 0)
   * @NTLM2_MALLOC_ERROR: memory allocation failure
   * @NTLM2_CALL_ERROR: application called the library with bad parameters
   * @NTLM2_TYPE2_INVALID: the received Type-2 message was invalid
   *
   * Library interface return codes.
   */
  typedef enum ntlm2_rc
  {
    NTLM2_OK = 0,
    NTLM2_MALLOC_ERROR = -1,
    NTLM2_CALL_ERROR = -2,
    NTLM2_TYPE2_INVALID = -3,
  } ntlm2_rc;

  /**
   * ntlm2_client_options:
   * @NTLM2_NO_TYPE2_VALIDATION: disable non-essential Type-2 validation
   *
   * Flags modifying client authentication.
   */
  typedef enum ntlm2_client_options
  {
    NTLM2_NO_TYPE2_VALIDATION = 1,
  } ntlm2_client_options;

  /**
   * NTLM2_HASH_LEN
   *
   * Length of (NT)LM-hashes.  The output buffers for ntlm2_lmhash()
   * and ntlm2_ntlmhash() needs to be at least this size.
   */
#define NTLM2_HASH_LEN 16

  /**
   * ntlm2c_t
   *
   * Client handle for NTLM authentication.
   */
  typedef struct ntlm2c_st *ntlm2c_t;

  /* Library global init/done functions. */
  extern int ntlm2_global_init (int options);
  extern void ntlm2_global_done (void);

  /* Low-level (NT)LM hash generator. */
  extern int ntlm2_lmhash (const char *passwd, char *out);
  extern int ntlm2_ntlmhash (const char *passwd, char *out);

  /* Client functions. */
  extern int ntlm2_client_init (ntlm2c_t * auth, int options);
  extern void ntlm2_client_done (ntlm2c_t auth);

  extern int ntlm2c_domain (ntlm2c_t auth, const char *domain);
  extern int ntlm2c_workstation (ntlm2c_t auth, const char *workstation);
  extern int ntlm2c_username (ntlm2c_t auth, const char *username);
  extern int ntlm2c_target (ntlm2c_t auth, const char *target);

  extern int ntlm2c_type1 (ntlm2c_t auth, char **out, size_t * len);
  extern int ntlm2c_type2 (ntlm2c_t auth, const char *type2, size_t type2len);
  extern int ntlm2c_type3_passwd (ntlm2c_t auth, const char *passwd,
				  char **out, size_t * len);

# ifdef __cplusplus
}
# endif

#endif
