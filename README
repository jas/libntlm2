Libntlm2 is an implementation of the NTLM authentication protocol
released under a dual BSD/LGPL license (see COPYING).  It was written
from scratch by Simon Josefsson following this documentation:

  http://davenport.sourceforge.net/ntlm.html

Testing made sure that it is fully backwards compatible with Libntlm.

WARNING!  Only use NTLM for interoperability purposes.  The protocol
provides weak security and is based on old cryptographical techniques.
It is only marginally better than sending your password in the clear.

Libntlm2 does not support all variants of NTLM.  In fact, Libntlm2
only sends LM+NTLM responses.  The LM response is weak, using only
DES.  The NTLM responses is stronger but still weak (MD4+DES).  Note
that both LM and NTLM responses are sent, thus the combined protocol
is only as secure as LM (i.e., not at all).

Libntlm2 does not support NTLM2 session responses nor NTLMv2.
Libntlm2 does not support sending only the NTLM response, to avoid the
weaknesses in the LM response.  Supporting more variants would be
fairly straight-forward to add if someone is interested.

Paid support and custom development of Libntlm2 is available through
the authors' company, Simon Josefsson Datakonsult AB, a Stockholm
based privately held company.  For more information see:

  http://josefsson.org/

To bootstrap the library from version controlled sources do:

  autoreconf -i

To build the library do:

  ./configure
  make
  make check
  make install

If you have any questions, please join the mailing list:

  http://lists.nongnu.org/mailman/listinfo/libntlm

Happy hacking!
