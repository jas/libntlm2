/* Copyright (C) 2010 Simon Josefsson
 * Dual-licensed under BSD/LGPLv2+, see COPYING for details.
 */

#include "des.h"

#include <stdio.h>

#define TEST(k, p, c) {							\
    uint64_t key = k, pt = p, expected = c, tmp;			\
    tmp = _ntlm2_des_encrypt (key, pt);					\
    if (tmp != expected)						\
      {									\
	printf ("DES(key=%llx, plaintext=%llx) = %llx"			\
		" expected %llx\n", key, pt, tmp, expected);		\
	return 1;							\
      }									\
  }

int
main (void)
{
  TEST (0x0000000000000000LL, 0x0000000000000000LL, 0x8CA64DE9C1B123A7LL);
  TEST (0xFFFFFFFFFFFFFFFFLL, 0xFFFFFFFFFFFFFFFFLL, 0x7359B2163E4EDC58LL);
  TEST (0x0123456789ABCDEFLL, 0x1111111111111111LL, 0x17668DFC7292532DLL);
  TEST (0x1111111111111111LL, 0x0123456789ABCDEFLL, 0x8A5AE1F81AB8F2DDLL);

  return 0;
}
